from math import floor


# # -- part one --
# frequency : int = 0
# with open('input_a', mode = 'r') as fStream:
#     for line in fStream.readlines():
#         frequency_change = int(line[:-1])
#         frequency += frequency_change
#
# print(frequency)


# -- part two --
frequencies : set[int] = {0}
frequency_changes : list[int] = []
with open('input_a', mode = 'r') as fStream:
    for line in fStream.readlines(): frequency_changes.append(int(line[:-1]))

current_frequency = 0
for idx in range(100_000):
    print(idx)
    for frequency_change in frequency_changes:
        current_frequency += frequency_change
        if current_frequency in frequencies: break
        frequencies.add(current_frequency)
    else: continue
    break

print(current_frequency)
