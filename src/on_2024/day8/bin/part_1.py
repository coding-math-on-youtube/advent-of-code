import pathlib as pl

import json


def solution(path_src: pl.Path) -> int:
    with open(path_src, mode = "r", encoding = "utf-8") as in_stream:
        lines: list[str] = in_stream.read().split("\n")[:-1]

    result: int = 0

    ...

    return result


def main():
    with open("../data/meta.json", mode = "r", encoding = "utf-8") as in_stream:
        meta = json.load(in_stream)

    # input for test case
    path_test: pl.Path = pl.Path(f'../data/{meta["test"]["part"]["1"]}')
    expected_result_test: int = meta["test"]["expected_result"]["1"]

    # actual input of the day
    path_input: pl.Path = pl.Path(f"../data/{meta['input']}")

    # exec
    if meta["test"]["exec"]:
        result_test: int = solution(path_src = path_test)
        if result_test != expected_result_test:
            print(f"result:   {result_test}")
            print(f"expected: {expected_result_test}")
            raise AssertionError("!")
        else: print("test successful")
    else:
        result: int = solution(path_src = path_input)
        print(f"answer for part_1 is: {result}")


if __name__ == "__main__": main()
