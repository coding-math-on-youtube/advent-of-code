import pathlib as pl


def _inner_check(digits) -> bool:
    if len(digits) <= 1:
        print("[WARNING] short report")
        return True

    orientation: int = 0
    if digits[0] < digits[1]: orientation = 1
    elif digits[0] > digits[1]: orientation = -1
    if orientation == 0: return False

    prec = digits[0]
    for succ in digits[1:]:
        seq: int = orientation*(succ - prec)
        prec = succ

        if (seq < 1) or (seq > 3): return False

    return True


def safety_check(digits: list[int]) -> bool:
    if _inner_check(digits): return True
    for idx in range(len(digits)):
        digits_cpy = list(digits)
        digits_cpy.pop(idx)
        if _inner_check(digits_cpy): return True
    return False


def main():
    data: pl.Path = pl.Path("../data/input.txt")
    # data: pl.Path = pl.Path("../data/test.txt")

    with open(data, mode = "r") as in_stream:
        lines = in_stream.read().split("\n")[:-1]

    count: int = 0
    for line in lines:
        digits = [int(digit) for digit in line.split(" ")]

        is_safe = safety_check(digits)
        if is_safe: count += 1
    print(f"answer part 2: {count}")


if __name__ == "__main__": main()
