import pathlib as pl


def main():
    data: pl.Path = pl.Path("../data/input.txt")
    # data: pl.Path = pl.Path("../data/test.txt")

    with open(data, mode = "r") as in_stream:
        lines = in_stream.read().split("\n")[:-1]

    count: int = 0
    for line in lines:
        digits = [int(digit) for digit in line.split(" ")]

        if len(digits) <= 1:
            count += 1
            print("[WARNING] short report")
            continue

        orientation: int = 0
        if digits[0] < digits[1]: orientation = 1
        elif digits[0] > digits[1]: orientation = -1
        if orientation == 0: continue

        prec = digits[0]
        for succ in digits[1:]:
            seq: int = orientation*(succ - prec)
            prec = succ

            if (seq < 1) or (seq > 3): break
        else: count += 1
    print(f"answer part 1: {count}")


if __name__ == "__main__": main()
