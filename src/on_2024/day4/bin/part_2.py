import pathlib as pl

import json


class WordMatrix(object):
    width: int
    height: int
    mat: list[str]

    def __init__(self, matrix: list[str]):
        self.height = len(matrix)
        self.width = len(matrix[0])
        self.mat = matrix

        for line in self.mat:
            if len(line) != self.width: raise AssertionError("!")

    def __getitem__(self, coord: tuple[int, int]) -> str | None:
        i, j = coord
        if i < 0: return None
        if j < 0: return None
        if i >= self.height: return None
        if j >= self.width: return None
        return self.mat[i][j]

    def cross_search(self, i: int, j: int, word: str) -> bool:
        if len(word) != 3: raise AssertionError("!")

        cond_a: bool = False
        for start, direction in [
            ((-1, -1), (1, 1)),
            ((1, 1), (-1, -1))
        ]:
            ii = i + start[0]
            jj = j + start[1]
            for letter in word:
                if self[ii, jj] != letter: break
                ii += direction[0]
                jj += direction[1]
            else: cond_a = True
            if cond_a: break
        else: return False

        cond_b: bool = False
        for start, direction in [
            ((-1, 1), (1, -1)),
            ((1, -1), (-1, 1))
        ]:
            ii = i + start[0]
            jj = j + start[1]
            for letter in word:
                if self[ii, jj] != letter: break
                ii += direction[0]
                jj += direction[1]
            else: cond_b = True
            if cond_b: break
        else: return False

        return cond_a and cond_b


def solution(path_src: pl.Path) -> int:
    with open(path_src, mode = "r", encoding = "utf-8") as in_stream:
        lines: list[str] = in_stream.read().split("\n")[:-1]

    result: int = 0

    wm = WordMatrix(lines)
    for i in range(1, wm.height - 1):
        for j in range(1, wm.width - 1):
            if (i, j) == (2, 6):
                print("")
            if wm[i, j] == "A":
                tmp = wm.cross_search(i, j, "MAS")
                if tmp: result += 1

    return result


def main():
    with open("../data/meta.json", mode = "r", encoding = "utf-8") as in_stream:
        meta = json.load(in_stream)

    # input for test case
    path_test: pl.Path = pl.Path(f'../data/{meta["test"]["part"]["2"]}')
    expected_result_test: int = meta["test"]["expected_result"]["2"]

    # actual input of the day
    path_input: pl.Path = pl.Path(f"../data/{meta['input']}")

    # exec
    if meta["test"]["exec"]:
        result_test: int = solution(path_src = path_test)
        if result_test != expected_result_test:
            print(f"result:   {result_test}")
            print(f"expected: {expected_result_test}")
            raise AssertionError("!")
        else: print("test successful")
    else:
        result: int = solution(path_src = path_input)
        print(f"answer for part_1 is: {result}")


if __name__ == "__main__": main()
