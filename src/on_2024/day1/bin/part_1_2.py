import pathlib as pl


def main():
    data: pl.Path = pl.Path("../data/input.txt")

    with open(data, mode = "r") as in_stream:
        lines = in_stream.read().split("\n")[:-1]
    left, right = list(), list()
    for line in lines:
        left_number, right_number = line.split("   ")
        left.append(int(left_number))
        right.append(int(right_number))

    # part one
    left.sort(), right.sort()
    dist = [abs(a - b) for a, b in zip(left, right)]
    print(f"answer part 1: {sum(dist)}")

    # part two
    out: int = 0
    for l in left: out += l*right.count(l)
    print(f"answer part 2: {out}")


if __name__ == "__main__": main()
