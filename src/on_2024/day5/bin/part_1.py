import pathlib as pl

import json

from typing import Self


class Rules(object):
    later: dict[int, set[int]]
    before: dict[int, set[int]]

    @classmethod
    def parse(cls, lines: list[str]) -> Self:
        out: Self = cls()
        for line in lines:
            if line.strip() == "": break
            left, right = line.split("|")
            out.append(int(left), int(right))
        return out

    def __init__(self):
        self.later = dict()
        self.before = dict()

    def append(self, first: int, second: int):
        if not (first in self.later): self.later[first] = set()
        self.later[first].add(second)
        if not (second in self.before): self.before[second] = set()
        self.before[second].add(first)

    def check_row(self, row: list[int]) -> bool:
        for idx, number in enumerate(row):
            if number in self.later.keys():
                for b_num in row[:idx]:
                    if b_num in self.later[number]: return False
            if number in self.before.keys():
                for l_num in row[idx + 1:]:
                    if l_num in self.before[number]: return False
        return True


def solution(path_src: pl.Path) -> int:
    with open(path_src, mode = "r", encoding = "utf-8") as in_stream:
        lines: list[str] = in_stream.read().split("\n")[:-1]

    result: int = 0

    r: Rules = Rules.parse(lines)
    skip: bool = True
    for line in lines:
        if line.strip() == "":
            skip = False
            continue
        if skip: continue
        row: list[int] = [int(num) for num in line.strip().split(",")]
        if not r.check_row(row): continue
        if (len(row) % 2) != 1: raise AssertionError("!")
        result += row[len(row)//2]

    return result


def main():
    with open("../data/meta.json", mode = "r", encoding = "utf-8") as in_stream:
        meta = json.load(in_stream)

    # input for test case
    path_test: pl.Path = pl.Path(f"../data/{meta['test']['part']['1']}")
    expected_result_test: int = meta["test"]["expected_result"]["1"]

    # actual input of the day
    path_input: pl.Path = pl.Path(f"../data/{meta['input']}")

    # exec
    if meta["test"]["exec"]:
        result_test: int = solution(path_src = path_test)
        if result_test != expected_result_test:
            print(f"result:   {result_test}")
            print(f"expected: {expected_result_test}")
            raise AssertionError("!")
        else: print("test successful")
    else:
        result: int = solution(path_src = path_input)
        print(f"answer for part_1 is: {result}")


if __name__ == "__main__": main()
