import pathlib as pl

import json

from typing import Self, Any, Callable, TypeVar, Generic
from collections.abc import Iterator


# T = TypeVar("T")


# class AStar(Generic[T]):
#     open_list: list[T]
#     closed_list: list[T]
#
#     neighbours: Callable[[T], Generator[T]]
#     cond_termination: Callable[[T], bool]
#
#     def __init__(
#         self,
#         start: T,
#         neighbours: Callable[[T], Generator[T]],
#         cond_termination: Callable[[T], bool]
#     ):
#         self.open_list = [start]
#         self.closed_list = list()
#
#         self.neighbours = neighbours
#         self.cond_termination = cond_termination
#
#     def search(self):
#         while len(self.open_list) > 0:
#             cur: T = self.open_list.pop(0)
#             if self.cond_termination(cur):
#                 return ...
#             self.closed_list.append(cur)
#             self.expand(cur)
#         return None
#
#     def expand(self, cur: T) -> None:
#         for neighbour in self.neighbours(cur):
#             if neighbour in self.closed_list: continue
#             tentative_g = self.g(cur) + self.c(cur, neighbour)


class Rules(object):
    later: dict[int, set[int]]
    before: dict[int, set[int]]

    @classmethod
    def parse(cls, lines: list[str]) -> Self:
        out: Self = cls()
        for line in lines:
            if line.strip() == "": break
            left, right = line.split("|")
            out.append(int(left), int(right))
        return out

    def __init__(self):
        self.later = dict()
        self.before = dict()

    def append(self, first: int, second: int):
        if not (first in self.later): self.later[first] = set()
        self.later[first].add(second)
        if not (second in self.before): self.before[second] = set()
        self.before[second].add(first)

    def check_row(self, row: list[int] | tuple[int, ...]) -> int:
        violations: int = 0
        for idx, number in enumerate(row):
            if number in self.later.keys():
                for b_num in row[:idx]:
                    if b_num in self.later[number]: violations += 1
            if number in self.before.keys():
                for l_num in row[idx + 1:]:
                    if l_num in self.before[number]: violations += 1
        return violations


def neighbours_of(row: tuple[int, ...]) -> Iterator[tuple[int, ...]]:
    for idx, entry in enumerate(row):
        for jdx, other in enumerate(row[idx + 1:], start = idx + 1):
            out = list(row)
            out[idx], out[jdx] = other, entry
            yield tuple(out)


class Search(object):
    r: Rules
    neighbours: Callable[[tuple[int, ...]], Iterator[tuple[int, ...]]]

    catalogue: list[list[tuple[int, ...]]]
    closed: list[tuple[int, ...]]

    def __init__(
        self,
        r: Rules,
        neighbours: Callable[[tuple[int, ...]], Iterator[tuple[int, ...]]]
    ):
        self.r = r
        self.neighbours = neighbours

        self.catalogue = []
        self.closed = list()

    def append(self, node: tuple[int, ...]) -> int:
        c: int = self.r.check_row(node)
        if c >= len(self.catalogue): self.catalogue.extend([list() for _ in range(c + 1 - len(self.catalogue))])
        self.catalogue[c].append(node)
        return c

    def expand(self, cur: tuple[int, ...]) -> bool:
        for neighbour in neighbours_of(cur):
            c: int = self.append(neighbour)
            if c == 0: return True
            self.closed.append(cur)

        keep_on: bool = True
        while keep_on:
            keep_on = False
            for candidates in self.catalogue:
                if len(candidates) == 0: continue
                for candidate in candidates:
                    if candidate in self.closed: continue
                    if self.expand(candidate): return True
                    keep_on = True

        return False


def solution(path_src: pl.Path) -> int:
    with open(path_src, mode = "r", encoding = "utf-8") as in_stream:
        lines: list[str] = in_stream.read().split("\n")[:-1]

    result: int = 0

    r: Rules = Rules.parse(lines)
    skip: bool = True
    for line in lines:
        if line.strip() == "":
            skip = False
            continue
        if skip: continue
        row: list[int] = [int(num) for num in line.strip().split(",")]
        c: int = r.check_row(row)
        if 0 == c: continue

        s: Search = Search(r, neighbours_of)
        s.expand(tuple(row))
        row = list(s.catalogue[0][0])

        result += row[len(row)//2]

    return result


def main():
    with open("../data/meta.json", mode = "r", encoding = "utf-8") as in_stream:
        meta = json.load(in_stream)

    # input for test case
    path_test: pl.Path = pl.Path(f"../data/{meta['test']['part']['2']}")
    expected_result_test: int = meta["test"]["expected_result"]["2"]

    # actual input of the day
    path_input: pl.Path = pl.Path(f"../data/{meta['input']}")

    # exec
    if meta["test"]["exec"]:
        result_test: int = solution(path_src = path_test)
        if result_test != expected_result_test:
            print(f"result:   {result_test}")
            print(f"expected: {expected_result_test}")
            raise AssertionError("!")
        else: print("test successful")
    else:
        result: int = solution(path_src = path_input)
        print(f"answer for part_1 is: {result}")


if __name__ == "__main__": main()
