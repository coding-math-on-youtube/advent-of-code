import pathlib as pl

import json

from typing import Self


class Pos(object):
    x: int
    y: int

    def copy(self) -> Self: return self.__class__(x = self.x, y = self.y)

    def __init__(self,x: int, y: int): self.x, self.y = x, y

    def __str__(self) -> str: return f"(x:{self.x}, y:{self.y})"

    def __repr__(self) -> str: return self.__str__()

    def __eq__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__): return False
        if self.x != other.x: return False
        if self.y != other.y: return False
        return True

    def to_tuple(self) -> tuple[int, int]: return self.x, self.y


class Directions:
    up: Pos = Pos(x = 0, y = -1)
    left: Pos = Pos(x = -1, y = 0)
    down: Pos = Pos(x = 0, y = 1)
    right: Pos = Pos(x = 1, y = 0)

    @classmethod
    def rotate_right(cls, direction: Pos):
        if direction == cls.up: return cls.right
        elif direction == cls.right: return cls.down
        elif direction == cls.down: return cls.left
        elif direction == cls.left: return cls.up
        raise AssertionError("!")


class Map(object):
    width: int
    height: int
    obstructions: list[Pos]

    def copy(self) -> Self:
        out: Self = self.__class__(width = self.width, height = self.height)
        for pos in self.obstructions: out.add_obs(pos.copy())
        return out

    @classmethod
    def parse(cls, lines: list[str]) -> tuple[Self, Pos, Pos]:
        if len(lines) == 0: raise AssertionError("!")
        height: int = len(lines)
        if len(lines[0]) == 0: raise AssertionError("!")
        width: int = len(lines[0])

        starting_position: Pos | None = None
        starting_direction: Pos | None = None

        out: Self = cls(width = width, height = height)
        for y_coord, line in enumerate(lines):
            if len(line) != width: raise AssertionError("!")
            for x_coord, char in enumerate(line):
                if char == "#": out.obstructions.append(Pos(x = x_coord, y = y_coord))
                elif char in "^<>v":
                    starting_position = Pos(x = x_coord, y = y_coord)
                    if not (starting_direction is None): raise AssertionError("!")
                    elif char == "^": starting_direction = Directions.up
                    elif char == ">": starting_direction = Directions.right
                    elif char == "<": starting_direction = Directions.left
                    elif char == "v": starting_direction = Directions.down
                    else: raise AssertionError("!")

        return out, starting_position, starting_direction

    def __str__(self) -> str:
        out: list[list[str]] = [["." for _ in range(self.width)] for _ in range(self.height)]
        for pos in self.obstructions:
            out[pos.y][pos.x] = "#"
        return "\n".join(["".join(line) for line in out])

    def __repr__(self) -> str: return self.__str__()

    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height
        self.obstructions = list()

    def add_obs(self, pos: Pos): self.obstructions.append(pos)

    def bounds_check(self, pos: Pos) -> bool:
        if (pos.x < 0) or (pos.x >= self.width): return False
        if (pos.y < 0) or (pos.y >= self.height): return False
        return True

    def check(self, pos: Pos, direction: Pos) -> None | tuple[Pos, Pos]:
        if not self.bounds_check(pos): return None

        rotated: Pos = Directions.rotate_right(direction)  # checks if direction actually comes from Directions class

        if pos in self.obstructions: raise AssertionError("!")
        new_pos: Pos = Pos(x = pos.x + direction.x, y = pos.y + direction.y)
        if new_pos in self.obstructions:
            direction = rotated
            pos, direction = self.check(pos, direction)
        else: pos = new_pos

        if self.bounds_check(pos): return pos, direction
        return None

    def does_loop(self, s: Pos, d_s: Pos) -> bool:
        pos_dirs: set[tuple[tuple[int, int], tuple[int, int]]] = set()
        pos_dirs.add((s.to_tuple(), d_s.to_tuple()))
        p: Pos = s
        d: Pos = d_s
        while True:
            resp = self.check(p, d)
            if resp is None: break
            p, d = resp
            pos_dir: tuple[tuple[int, int], tuple[int, int]] = (p.to_tuple(), d.to_tuple())
            if pos_dir in pos_dirs: return True
            pos_dirs.add(pos_dir)
        return False


def solution(path_src: pl.Path) -> int:
    with open(path_src, mode = "r", encoding = "utf-8") as in_stream:
        lines: list[str] = in_stream.read().split("\n")[:-1]

    m, s, d_s = Map.parse(lines)  # s - start pos; d_s - start direction
    positions: set[tuple[int, int]] = set()
    positions.add(s.to_tuple())
    p: Pos = s
    d: Pos = d_s
    while True:
        resp = m.check(p, d)
        if resp is None: break
        p, d = resp
        positions.add(p.to_tuple())

    result: int = 0
    for idx, pos_tuple in enumerate(positions):
        print(f"{100*((idx + 1)/(len(positions) - 1)):6.2f} %")
        if pos_tuple == s.to_tuple(): continue
        m_cpy: Map = m.copy()
        m_cpy.add_obs(Pos(x = pos_tuple[0], y = pos_tuple[1]))
        if m_cpy.does_loop(s, d_s): result += 1

    return result


def main():
    with open("../data/meta.json", mode = "r", encoding = "utf-8") as in_stream:
        meta = json.load(in_stream)

    # input for test case
    path_test: pl.Path = pl.Path(f'../data/{meta["test"]["part"]["2"]}')
    expected_result_test: int = meta["test"]["expected_result"]["2"]

    # actual input of the day
    path_input: pl.Path = pl.Path(f"../data/{meta['input']}")

    # exec
    if meta["test"]["exec"]:
        result_test: int = solution(path_src = path_test)
        if result_test != expected_result_test:
            print(f"result:   {result_test}")
            print(f"expected: {expected_result_test}")
            raise AssertionError("!")
        else: print("test successful")
    else:
        result: int = solution(path_src = path_input)
        print(f"answer for part_1 is: {result}")


if __name__ == "__main__": main()
