from typing import Union


RAW_ex: str = '''vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
'''

SOL_ex_part_1: int = 157

SOL_ex_part_2: int = 70


with open('input', mode = 'r') as fStream: RAW: str = fStream.read()
