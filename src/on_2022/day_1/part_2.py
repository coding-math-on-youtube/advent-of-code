from on_2022.day_1.source_loader import RAW_ex, RAW, SOL_ex_part_2


def part_2(raw: str):
    summ_curr: int = 0
    idx_curr: int = 1
    idx_tmp: int = -1
    summ_maxs: list[int] = [-1, -1, -1]
    idx_maxs: list[int] = [-1, -1, -1]
    for line in raw.split('\n'):
        try: summ_curr += int(line)
        except ValueError:
            idx_tmp = idx_curr
            for pos in range(3):
                if summ_curr > summ_maxs[pos]:
                    summ_maxs[pos], summ_curr = summ_curr, summ_maxs[pos]
                    idx_maxs[pos], idx_tmp = idx_tmp, idx_maxs[pos]
            summ_curr = 0
            idx_curr += 1

    return sum(summ_maxs), summ_maxs, idx_maxs


def test():
    out = part_2(RAW_ex)
    assert out[0] == SOL_ex_part_2[0]
    assert tuple(out[2]) == SOL_ex_part_2[1]


def main():
    test()
    print(part_2(RAW))


if __name__ == '__main__': main()
