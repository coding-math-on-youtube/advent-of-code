from typing import Union


RAW_ex: str = '''1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
'''

SOL_ex_part_1: tuple[int, int] = (24000, 4)

SOL_ex_part_2: tuple[int, tuple[int, int, int]] = (45000, (4, 3, 5))


with open('input', mode = 'r') as fStream: RAW: str = fStream.read()
