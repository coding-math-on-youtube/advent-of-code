from on_2022.day_2.source_loader import RAW_ex, SOL_ex_part_1, RAW
from on_2022.day_2.source_loader import ROCK, PAPER, SCISSORS, simulate_round
from on_2022.day_2.source_loader import points, RIGHT, WIN, LEFT, LOOSE, DRAW


figure_map = dict(A = ROCK, B = PAPER, C = SCISSORS, X = ROCK, Y = PAPER, Z = SCISSORS)


def part_1(raw: str) -> int:
    rights_win_condtion = {RIGHT: WIN, LEFT: LOOSE, DRAW: DRAW}

    score: int = 0
    for line in raw.splitlines():
        left, right = line.split(' ')

        left = figure_map[left]
        right = figure_map[right]

        score += points[right]
        _, _, round_result = simulate_round(left = left, right = right)
        score += points[rights_win_condtion[round_result]]
    return score


def test():
    out = part_1(RAW_ex)
    assert out == SOL_ex_part_1


def main():
    test()
    print(part_1(RAW))


if __name__ == '__main__': main()
