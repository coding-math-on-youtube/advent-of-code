from on_2022.day_7.source_loader import RAW_ex, SOL_ex_part_2, RAW, Folder, File, parse_terminal_out


SIZE: int = 70_000_000
MIN_REQ: int = 30_000_000
SAFE_SIZE: int = SIZE - MIN_REQ


def recursive_find_suitable_deletion(top: Folder, curr: Folder, curr_candidate: Folder) -> Folder:
    if top.size() - curr.size() <= SAFE_SIZE:
        if curr.size() < curr_candidate.size(): curr_candidate = curr
    else: return curr_candidate
    for c in curr.contents:
        if isinstance(c, Folder):
            curr_candidate = recursive_find_suitable_deletion(top = top, curr = c, curr_candidate = curr_candidate)
    return curr_candidate


def part_2(raw: str) -> int:
    top: Folder = parse_terminal_out(raw)
    best_option: Folder = recursive_find_suitable_deletion(top, top, top)
    return best_option.size()


def test():
    out = part_2(RAW_ex)
    assert out == SOL_ex_part_2


def main():
    test()
    print(part_2(RAW))


if __name__ == '__main__': main()
