from on_2022.day_7.source_loader import RAW_ex, SOL_ex_part_1, RAW, Folder, File, parse_terminal_out


def recursive_size_determination(curr: Folder, max_cap: int = -1) -> int:
    total: int = 0
    if (max_cap > 0) and (curr.size() <= max_cap): total += curr.size()
    for c in curr.contents:
        if isinstance(c, Folder):
            total += recursive_size_determination(c, max_cap = max_cap)
    return total


def part_1(raw: str) -> int:
    top: Folder = parse_terminal_out(raw)
    return recursive_size_determination(top, max_cap = 100_000)


def test():
    out = part_1(RAW_ex)
    assert out == SOL_ex_part_1


def main():
    test()
    print(part_1(RAW))


if __name__ == '__main__': main()
