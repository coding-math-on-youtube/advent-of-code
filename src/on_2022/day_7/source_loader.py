from typing import Protocol, Union


class Node(Protocol):
    parent: Union["Folder", None]

    name: str

    def size(self) -> int: ...

    def get_str_indent(self) -> int: ...

    def __str__(self) -> str: ...

    def __repr__(self) -> str: ...


class File(object):
    parent: "Folder"

    name: str

    _size: int

    def __init__(self, *, name: str, size: int, parent: "Folder"):
        self.parent = parent
        self.name = name
        self._size = size

        self.parent.contents.append(self)

    def size(self) -> int: return self._size

    def get_str_indent(self) -> int:
        if self.parent is None: return 0
        return 2 + self.parent.get_str_indent()

    def __str__(self) -> str:
        out: str = f'{" "*self.get_str_indent()}- {self.name} (file, size = {self.size()})'
        return out

    def __repr__(self) -> str: return self.__str__()


class Folder(object):
    parent: Union["Folder", None]

    name: str

    contents: list["Folder", File]

    def __init__(self, *, name: str, parent: Union["Folder", None]):
        self.parent = parent
        self.name = name
        self.contents = []

        if not (self.parent is None): self.parent.contents.append(self)

    def size(self) -> int:
        sz: int = 0
        for c in self.contents: sz += c.size()
        return sz

    def find(self, name: str) -> "Folder":
        for c in self.contents:
            if isinstance(c, Folder) and (c.name == name): return c
        raise FileNotFoundError('!')

    def get_str_indent(self) -> int:
        if self.parent is None: return 0
        return 2 + self.parent.get_str_indent()

    def __str__(self) -> str:
        out: str = f'{" "*self.get_str_indent()}- {self.name} (dir)'
        for c in self.contents: out += '\n' + str(c)
        return out

    def __repr__(self) -> str: return self.__str__()


def parse_terminal_out(raw: str) -> Folder:
    lines = raw.splitlines()
    assert lines[0] == "$ cd /"
    top: Folder = Folder(name = "/", parent = None)
    current_folder: Folder = top

    for line in lines[1:]:
        if line == "$ ls": pass
        elif line[:4] == "$ cd":
            _, _, folder_name = line.split(' ')
            if folder_name == '..': current_folder = current_folder.parent
            else: current_folder = current_folder.find(name = folder_name)
        elif line[:3] == "dir":
            _, folder_name = line.split(' ')
            Folder(name = folder_name, parent = current_folder)
        elif line[0] == '$': raise AssertionError('!')
        else:
            size, file_name = line.split()
            File(name = file_name, size = int(size), parent = current_folder)

    return top


RAW_ex: str = '''$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
'''

SOL_ex_part_1: int = 95437

SOL_ex_part_2: int = 24933642


with open('input', mode = 'r') as fStream: RAW: str = fStream.read()
