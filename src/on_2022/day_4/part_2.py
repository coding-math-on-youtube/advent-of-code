from on_2022.day_4.source_loader import RAW_ex, SOL_ex_part_2, RAW


def part_2(raw: str) -> int:
    overlapping: int = 0
    for line in raw.splitlines():
        left, right = line.split(',')
        a, b = left.split('-')
        c, d = right.split('-')
        aa, bb, cc, dd = int(a), int(b), int(c), int(d)
        if (aa <= cc) and (bb >= cc): overlapping += 1
        elif (aa <= dd) and (bb >= dd): overlapping += 1
        elif (cc <= aa) and (dd >= aa): overlapping += 1
        elif (cc <= bb) and (dd >= bb): overlapping += 1

    return overlapping


def test():
    out = part_2(RAW_ex)
    assert out == SOL_ex_part_2


def main():
    test()
    print(part_2(RAW))


if __name__ == '__main__': main()
