from on_2022.day_4.source_loader import RAW_ex, SOL_ex_part_1, RAW


def part_1(raw: str) -> int:
    contained: int = 0
    for line in raw.splitlines():
        left, right = line.split(',')
        a, b = left.split('-')
        c, d = right.split('-')
        aa, bb, cc, dd = int(a), int(b), int(c), int(d)
        if (aa >= cc) and (bb <= dd): contained += 1
        elif (cc >= aa) and (dd <= bb): contained += 1

    return contained


def test():
    out = part_1(RAW_ex)
    assert out == SOL_ex_part_1


def main():
    test()
    print(part_1(RAW))


if __name__ == '__main__': main()
