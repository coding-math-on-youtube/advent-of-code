from typing import Union


# '''
#     -- part one --
# '''
# number_of_increases : int = 0
# height : Union[int, None] = None
# with open('input_a', mode = 'r') as fStream:
#     for line in fStream.readlines():
#         new_height : int = int(line[:-1])
#         if height is None: pass
#         elif height < new_height:
#             number_of_increases += 1
#         height = new_height
# print(number_of_increases)


'''
    -- part two --
'''
measurements : list[int] = []
with open('input_a', mode = 'r') as fStream:
    for line in fStream.readlines():
        measurements.append(int(line[:-1]))

number_of_increases : int = 0
summed_height : Union[int, None] = None
for sliding_window in zip(measurements,
                          measurements[1:],
                          measurements[2:]):
    new_summed_height : int = sum(sliding_window)
    if summed_height is None: pass
    elif summed_height < new_summed_height:
        number_of_increases += 1
    summed_height = new_summed_height
print(number_of_increases)
