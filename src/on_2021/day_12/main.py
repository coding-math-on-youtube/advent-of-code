from typing import Union, Optional, Callable


RAW_ex: str = '''dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc
'''


class Graph(object):
    nodes_reg: dict[str, '_Node'] = None
    nodes: set[str] = None

    def __init__(self):
        self.nodes_reg = dict()
        self.nodes = set()

    def append(self, n: str, k: Optional[str] = None) -> None:
        if not (n in self.nodes):
            self.nodes_reg[n] = _Node(n)
            self.nodes.add(n)

        if not (k is None):
            if not (k in self.nodes):
                self.nodes_reg[k] = _Node(k)
                self.nodes.add(k)

            n = self.nodes_reg[n]
            k = self.nodes_reg[k]
            n.neighbours.add(k)
            k.neighbours.add(n)


class _Node(object):
    name: str = None
    neighbours: set['_Node'] = None

    def __init__(self, name: str):
        self.name = name
        self.neighbours = set()

    @property
    def is_big(self) -> bool: return self.name.isupper()

    def __str__(self) -> str: return self.name

    def __repr__(self) -> str: return f'_Node("{self.name}")'


def convert_raw_to_graph(lines: list[str]) -> Graph:
    g = Graph()
    for line in lines: g.append(*line.split('-'))
    return g


def find_paths(current_node: _Node,
               end: _Node,
               path_constraint: Callable[[tuple[_Node], _Node], bool],
               path: tuple[_Node] = None) -> set[tuple[_Node]]:  # depth-first search
    if path is None: path: tuple[_Node] = (current_node,)
    if current_node is end: return {path}
    out: set[tuple[_Node]] = set()
    for neighbour in current_node.neighbours:
        if path_constraint(path, neighbour):
            new_path = tuple(path)
            new_path += (neighbour,)
            out = out.union(find_paths(neighbour, end, path_constraint, new_path))
    return out


def part1_constraint(path: tuple[_Node], new_node: _Node) -> bool:
    for node in path:
        if (not node.is_big) and (node is new_node): return False
    return True


def part2_constraint(path: tuple[_Node], new_node: _Node) -> bool:
    exceptional_node: Union[_Node, None] = None
    small_cavern: set[_Node] = set()
    for node in path:
        if node.is_big: continue
        if node in small_cavern:
            if exceptional_node is None: exceptional_node = node
            else: return False
        small_cavern.add(node)
        if node is new_node:
            if new_node.name in ('start', 'end'): return False
            if exceptional_node is None: exceptional_node = new_node
            else: return False
    return True


def main():
    '''
        -- resources --
    '''
    g_ex = convert_raw_to_graph(RAW_ex.splitlines())
    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    g = convert_raw_to_graph(RAW.splitlines())

    '''
        -- part one --
    '''
    assert len(find_paths(g_ex.nodes_reg['start'], g_ex.nodes_reg['end'], part1_constraint)) == 19
    print(len(find_paths(g.nodes_reg['start'], g.nodes_reg['end'], part1_constraint)))

    '''
        -- part two --
    '''
    assert len(find_paths(g_ex.nodes_reg['start'], g_ex.nodes_reg['end'], part2_constraint)) == 103
    print(len(find_paths(g.nodes_reg['start'], g.nodes_reg['end'], part2_constraint)))


if __name__ == '__main__': main()
