from typing import Union, TypeAlias
import numpy as np


RAW_example = '''00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
'''


def bin_list_2_int(bin : list[int]):
    out: int = 0
    for b in bin: out = (out << 1) | b
    return out


class PowCon(object):
    balance: list[int] = None  # comparing gamma to epsilon rate

    def _first_bin(self, bin: list[int]) -> None:
        self.balance = []
        for b in bin: self.balance.append(1 if b else -1)

    def next_bin(self, bin: list[int]) -> None:
        if self.balance is None: return self._first_bin(bin)
        for idx, b in enumerate(bin):
            if b: self.balance[idx] += 1
            else: self.balance[idx] -= 1

    def check(self) -> bool:
        for b in self.balance:
            if b == 0: return False
        return True

    @property
    def gamma_rate(self) -> list[int]:
        assert self.check()
        return [1 if b > 0 else 0 for b in self.balance]

    @property
    def epsilon_rate(self) -> list[int]:
        assert self.check()
        return [0 if b > 0 else 1 for b in self.balance]

    @property
    def gamma_value(self) -> int:
        return bin_list_2_int(self.gamma_rate)

    @property
    def epsilon_value(self) -> int:
        return bin_list_2_int(self.epsilon_rate)



def main():
    pc_example = PowCon()
    for line in RAW_example.splitlines():
        pc_example.next_bin([int(b) for b in line])
    assert pc_example.gamma_value*pc_example.epsilon_value == 198

    pc = PowCon()
    with open('input_a', mode = 'r') as fStream:
        for line in fStream.readlines():
            pc.next_bin([int(b) for b in line[:-1]])
    print(pc.gamma_value*pc.epsilon_value)


if __name__ == '__main__': main()
