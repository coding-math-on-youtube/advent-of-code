import numpy as np

from typing import Union, TypeAlias


RAW_ex: str = '''1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581
'''


def raw_to_mat(raw: str) -> np.ndarray:
    return np.array([[int(entry) for entry in line] for line in raw.splitlines()], dtype = int)


def raw_to_5mat(raw: str) -> np.ndarray:
    mat: np.ndarray = raw_to_mat(raw)
    new_mat = np.vstack((np.hstack((mat,     mat + 1, mat + 2, mat + 3, mat + 4)),
                         np.hstack((mat + 1, mat + 2, mat + 3, mat + 4, mat + 5)),
                         np.hstack((mat + 2, mat + 3, mat + 4, mat + 5, mat + 6)),
                         np.hstack((mat + 3, mat + 4, mat + 5, mat + 6, mat + 7)),
                         np.hstack((mat + 4, mat + 5, mat + 6, mat + 7, mat + 8))))
    new_mat -= 1
    new_mat %= 9
    new_mat += 1
    return new_mat


node_t: TypeAlias = tuple[int, int]


def simple_dijkstra(mat: np.ndarray) -> Union[bool, dict[node_t, node_t]]:
    start: node_t = (0, 0)
    end: node_t = (mat.shape[0] - 1, mat.shape[1] - 1)
    open_list: list[node_t] = [start]  # node still to expans on
    # f costs, i.e. g + h costs  -aka-  (heuristically) guessed costs to goal
    g: dict[node_t, int] = {start: 0}  # g costs  -aka-  costs to get to nodes from start
    closed_set: set[node_t] = set()  # expanded nodes
    # or came_from dictionary to determine ideal moving options
    moving_options: dict[node_t, node_t] = {start: start}
    while open_list:
        open_list.sort(key = lambda arg: -g[arg])
        current_node = open_list.pop()
        if current_node == end: return moving_options
        closed_set.add(current_node)
        expand_on(current_node, mat, open_list, g, closed_set, moving_options)
    return False


def expand_on(current_node: node_t,
              mat: np.ndarray,
              open_list: list[node_t],
              g: dict[node_t, int],
              closed_set: set[node_t],
              moving_options: dict[node_t, node_t]) -> None:
    i, j = current_node
    neighbours: list[node_t] = []
    if i > 0: neighbours.append((i - 1, j))
    if i + 1 < mat.shape[0]: neighbours.append((i + 1, j))
    if j > 0: neighbours.append((i, j - 1))
    if j + 1 < mat.shape[1]: neighbours.append((i, j + 1))
    for neighbour in neighbours:
        if neighbour in closed_set: continue
        tentative_g: int = g[current_node] + mat[neighbour]
        neighbour_in_open: bool = neighbour in set(open_list)
        if neighbour_in_open and (g[neighbour] <= tentative_g): continue
        moving_options[neighbour] = current_node
        g[neighbour] = tentative_g
        if not neighbour_in_open: open_list.append(neighbour)


def gather_path(mat: np.ndarray) -> list[node_t]:
    moving_options = simple_dijkstra(mat)
    end: node_t = (mat.shape[0] - 1, mat.shape[1] - 1)
    path: list[node_t] = [end]
    while path[-1] != moving_options[path[-1]]: path.append(moving_options[path[-1]])
    return path


def total_risk_cost(mat: np.ndarray, path: list[node_t]) -> int:
    risk_cost: int = 0
    for node in path:
        if node == (0, 0): continue
        risk_cost += mat[node]
    return risk_cost


def path_print(mat: np.ndarray, path: list[node_t]) -> None:
    out: str = ''
    for i in range(mat.shape[0]):
        out += '\n'
        for j in range(mat.shape[1]):
            if (i, j) in path: out += 'x'
            else: out += str(mat[i, j])
    print(out)


def main():
    '''
        -- part one --
    '''
    arr_ex = raw_to_mat(RAW_ex)
    path_ex = gather_path(arr_ex)
    assert total_risk_cost(arr_ex, path_ex) == 40

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    arr = raw_to_mat(RAW)
    path = gather_path(arr)
    print(total_risk_cost(arr, path))

    '''
        -- part two --
    '''
    with open('input_b', mode = 'r') as fStream: RAW_ex_b: str = fStream.read()
    arr_ex_b = raw_to_5mat(RAW_ex)
    arr_ex_c = raw_to_mat(RAW_ex_b)
    assert np.sum(abs(arr_ex_c - arr_ex_b)) == 0
    path_ex_b = gather_path(arr_ex_b)
    assert total_risk_cost(arr_ex_b, path_ex_b) == 315

    arr_b = raw_to_5mat(RAW)
    path_b = gather_path(arr_b)
    print(total_risk_cost(arr_b, path_b))


if __name__ == '__main__': main()
