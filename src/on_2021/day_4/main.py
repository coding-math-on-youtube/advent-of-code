from typing import TypeAlias


RAW_example = '''7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7'''


class Board(object):
    data: list[list[int]]
    mask: list[list[int]]
    lucky_num: int
    has_won: bool = False

    def __init__(self, raw_board_as_lines: list[str]):
        self.data = [[int(entry) for entry in line.split()]
                     for line in raw_board_as_lines]
        self.mask = [[0 for _ in range(5)] for _ in range(5)]

    def draw(self, num: int | str) -> bool:
        if isinstance(num, str): num = int(num)
        updated: bool = False
        col_idx: int | None = None
        row_idx: int | None = None
        for _row_idx, (row, row_mask) in enumerate(zip(self.data, self.mask)):
            for _col_idx, entry in enumerate(row):
                if (entry == num) and (not row_mask[_col_idx]):
                    row_mask[_col_idx] = 1
                    updated = True
                    row_idx = _row_idx
                    col_idx = _col_idx
                    self.lucky_num = num
                    break
            else: continue
            break
        if updated and (not self.has_won):
            for row_mask in self.mask:  # check no.1
                if not row_mask[col_idx]: break
            else: self.has_won = True
            for entry in self.mask[row_idx]:  # check no.2
                if not entry: break
            else: self.has_won = True
        return self.has_won

    def __str__(self) -> str:
        out: str = ''
        for row, row_mask in zip(self.data, self.mask):
            out += '\n'
            out += str(row) + '\n'
            out += str(row_mask) + '\n'
        return out

    def sum_free(self) -> int:
        out: int = 0
        for row, row_mask in zip(self.data, self.mask):
            for entry, entry_mask in zip(row, row_mask):
                out += (1 - entry_mask)*entry
        return out


def craft_boards(lines: list[str]) -> tuple[list[str], list[Board]]:
    drawn_numbers = lines[0].split(',')
    boards: list[Board] = []
    _next_board: list[str] = []
    for line in lines[2:]:
        if line: _next_board.append(line)
        else:
            boards.append(Board(_next_board))  # ini board
            _next_board = []  # reset for new board
    if _next_board: boards.append(Board(_next_board))
    return drawn_numbers, boards


def play(drawn_numbers: list[str], boards: list[Board]) -> Board | None:
    first_winning_board: Board | None = None
    for drawn_number in drawn_numbers:
        for board in boards:
            if board.draw(drawn_number):
                first_winning_board = board
                break
        else: continue
        break
    return first_winning_board


def play_4_squid(drawn_numbers: list[str], boards: list[Board]) -> Board | None:
    last_winning_board: Board | None = None
    for drawn_number in drawn_numbers:
        for board in boards:
            board.draw(drawn_number)
            if all([_board.has_won for _board in boards]):
                last_winning_board = board
                break
        else: continue
        break
    return last_winning_board


def main():
    '''
        -- resources --
    '''
    lines_ex = RAW_example.splitlines()
    drawn_numbers_ex, boards_ex = craft_boards(lines_ex)

    with open('input_a', mode = 'r') as fStream: lines = fStream.read().splitlines()
    drawn_numbers, boards = craft_boards(lines)

    '''
        -- part one --
    '''
    first_winning_board_ex = play(drawn_numbers_ex, boards_ex)
    assert first_winning_board_ex.sum_free() == 188
    assert first_winning_board_ex.sum_free()*first_winning_board_ex.lucky_num == 4512

    first_winning_board = play(drawn_numbers, boards)
    print(first_winning_board.sum_free()*first_winning_board.lucky_num)

    '''
        -- part two --
    '''
    last_winning_board_ex = play_4_squid(drawn_numbers_ex, boards_ex)
    assert last_winning_board_ex.sum_free() == 148
    assert last_winning_board_ex.sum_free()*last_winning_board_ex.lucky_num == 1924

    last_winning_board = play_4_squid(drawn_numbers, boards)
    print(last_winning_board.sum_free()*last_winning_board.lucky_num)


if __name__ == '__main__': main()
