from typing import Union


RAW_ex = '''[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
'''

points_reg_task1: dict[str, int] = {')': 3, ']': 57, '}': 1197, '>': 25137}
points_reg_task2: dict[str, int] = {'(': 1, '[': 2, '{': 3, '<': 4}
reply_reg: dict[str, str] = {'(': ')', '[': ']', '{': '}', '<': '>'}
openers: str = '([{<'
closers: str = ')]}>'


def verify(line: str) -> tuple[list[str], list[tuple[int, str, Union[str, None]]]]:
    stack: list[str] = []
    errors: list[tuple[int, str, Union[str, None]]] = []
    for idx, lit in enumerate(line):
        if lit in openers: stack.append(lit)
        elif lit in closers:
            if len(stack) == 0: errors.append((idx, lit, None))
            else:
                expected: str = reply_reg[stack.pop()]
                if lit != expected: errors.append((idx, lit, expected))
        else:
            print(lit)
            raise AssertionError('?')
    return stack, errors


def task1(lines: list[str]) -> int:
    score: int = 0
    for line in lines:
        stack, errors = verify(line)
        if len(errors) > 0: score += points_reg_task1[errors[0][1]]
    return score


def task2(lines: list[str]) -> int:
    scores: list[int] = []
    for line in lines:
        stack, errors = verify(line)
        if len(errors) > 0: continue
        stack.reverse()
        score: int = 0
        for expected in stack: score = 5*score + points_reg_task2[expected]
        scores.append(score)
    scores.sort()
    return scores[len(scores)//2]


def main():
    '''
        -- resources --
    '''
    lines_ex = RAW_ex.splitlines()
    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    lines = RAW.splitlines()

    '''
        -- part one --
    '''
    assert task1(lines_ex) == 26397
    print(task1(lines))

    '''
        -- part two --
    '''
    assert task2(lines_ex) == 288957
    print(task2(lines))


if __name__ == '__main__': main()
