from typing import Union, TypeAlias


# debug
RAW_pre_ex_01: str = '''D2FE28'''
RAW_pre_ex_02: str = '''38006F45291200'''
RAW_pre_ex_03: str = '''EE00D40C823060'''

# actual examples
DATA_ex_01: tuple[tuple[str, int], ...] = (('''8A004A801A8002F478''', 16),
                                           ('''620080001611562C8802118E34''', 12),
                                           ('''C0015000016115A2E0802F182340''', 23),
                                           ('''A0016C880162017C3686B18A3D4780''', 31))

DATA_ex_02: tuple[tuple[str, int], ...] = (('''C200B40A82''', 3),
                                           ('''04005AC33890''', 54),
                                           ('''880086C3E88112''', 7),
                                           ('''CE00C43D881120''', 9),
                                           ('''D8005AC2A8F0''', 1),
                                           ('''F600BC2D8F''', 0),
                                           ('''9C005AC2F8F0''', 0),
                                           ('''9C0141080250320F1802104A08''', 1))


hex_2_bin_map: dict[str, str] = {'0': '0000', '1': '0001', '2': '0010', '3': '0011',
                                 '4': '0100', '5': '0101', '6': '0110', '7': '0111',
                                 '8': '1000', '9': '1001', 'A': '1010', 'B': '1011',
                                 'C': '1100', 'D': '1101', 'E': '1110', 'F': '1111'}


def hex_2_bin(message_hex: str) -> str:
    message_bin: str = ''
    for letter in message_hex: message_bin += hex_2_bin_map[letter]
    return message_bin


def bin_2_dec(binary: str) -> int:
    out: int = 0
    for o, b in enumerate(reversed(binary)):
        if b == '1': out += 2**o
    return out


out_struct_t: TypeAlias = dict[str, Union[int, str, list['out_struct_t']]]  # out_struct_t '==' packet


def lexer(message_bin: str, debug: bool = False) -> tuple[out_struct_t, str]:
    out: out_struct_t = {}
    if len(message_bin) < 6: raise AssertionError('!')
    out['V'], message_bin = bin_2_dec(message_bin[:3]), message_bin[3:]
    T, message_bin = bin_2_dec(message_bin[:3]), message_bin[3:]
    if T == 4:
        if debug: out['_T'] = T
        lit: str = ''
        while True:
            if len(message_bin) < 5: raise AssertionError('!')
            front, pack4, message_bin = message_bin[0], message_bin[1:5], message_bin[5:]
            lit += pack4
            if front == '0': break
        out['lit'] = bin_2_dec(lit)
    else:
        out['T'] = T
        if len(message_bin) < 1: raise AssertionError('!')
        I, message_bin = bin_2_dec(message_bin[0]), message_bin[1:]
        if debug: out['_I'] = I
        len_L: int = 15
        if I: len_L = 11
        if len(message_bin) < len_L: raise AssertionError('!')
        L, message_bin = bin_2_dec(message_bin[:len_L]), message_bin[len_L:]
        if debug: out['_L'] = L
        out['sub_structs'] = []
        if I:
            for _ in range(L):
                sub_out, message_bin = lexer(message_bin, debug)
                out['sub_structs'].append(sub_out)
        else:
            if len(message_bin) < L: raise AssertionError('!')
            message_bin, _rest = message_bin[:L], message_bin[L:]
            while message_bin:
                sub_out, message_bin = lexer(message_bin, debug)
                out['sub_structs'].append(sub_out)
            message_bin = _rest
    return out, message_bin


def sum_of_versions(out_struct: out_struct_t) -> int:
    out: int = out_struct['V']
    for sub_struct in out_struct.get('sub_structs', []): out += sum_of_versions(sub_struct)
    return out


def execute(out_struct: out_struct_t) -> int:
    if 'T' in out_struct:
        T: int = out_struct['T']
        match T:
            case 0: return sum([execute(sub_struct) for sub_struct in out_struct['sub_structs']])
            case 1:
                out: int = 1
                for sub_struct in out_struct['sub_structs']: out *= execute(sub_struct)
                return out
            case 2: return min([execute(sub_struct) for sub_struct in out_struct['sub_structs']])
            case 3: return max([execute(sub_struct) for sub_struct in out_struct['sub_structs']])
            case 5:
                if len(out_struct['sub_structs']) != 2: raise AssertionError('!')
                if execute(out_struct['sub_structs'][0]) > execute(out_struct['sub_structs'][1]): return 1
                return 0
            case 6:
                if len(out_struct['sub_structs']) != 2: raise AssertionError('!')
                if execute(out_struct['sub_structs'][0]) < execute(out_struct['sub_structs'][1]): return 1
                return 0
            case 7:
                if len(out_struct['sub_structs']) != 2: raise AssertionError('!')
                if execute(out_struct['sub_structs'][0]) == execute(out_struct['sub_structs'][1]): return 1
                return 0
            case _: raise AssertionError('!')
    return out_struct['lit']


def main():
    '''
        -- debug --
    '''
    print(lexer(hex_2_bin(RAW_pre_ex_01), True))
    print(lexer(hex_2_bin(RAW_pre_ex_02), True))
    # print(lexer(hex_2_bin(RAW_pre_ex_03), True))

    '''
        -- (some) resources & part one --
    '''
    for data_inst in DATA_ex_01:
        message_hex: str = data_inst[0]
        check_sum: int = data_inst[1]
        assert sum_of_versions(lexer(hex_2_bin(message_hex))[0]) == check_sum

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read().strip()
    print(sum_of_versions(lexer(hex_2_bin(RAW))[0]))

    '''
        -- part two --
    '''
    for data_inst in DATA_ex_02:
        message_hex: str = data_inst[0]
        check_sum: int = data_inst[1]
        assert execute(lexer(hex_2_bin(message_hex))[0]) == check_sum

    print(execute(lexer(hex_2_bin(RAW))[0]))


if __name__ == '__main__': main()
