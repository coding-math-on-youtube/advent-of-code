from typing import TypeAlias


RAW_ex: str = '''target area: x=20..30, y=-10..-5
'''

pos_t: TypeAlias = tuple[int, int]
span_t: TypeAlias = list[int]


def convert_raw_to_dict(raw: str) -> dict[str, span_t]:
    parts: list[str] = raw.split()
    span_x: span_t = [int(entry) for entry in parts[2].strip(',').split('=')[1].split('..')]
    span_y: span_t = [int(entry) for entry in parts[3].split('=')[1].split('..')]
    return dict(x = span_x, y = span_y)


def check_hit(pos: pos_t, target_area: dict[str, span_t]) -> bool:
    if (pos[0] < target_area['x'][0]) or (pos[0] > target_area['x'][1]): return False
    if (pos[1] < target_area['y'][0]) or (pos[1] > target_area['y'][1]): return False
    return True


def _single_shot_move(current_pos: pos_t, velo: pos_t) -> tuple[pos_t, pos_t]:
    target_pos: pos_t = (current_pos[0] + velo[0], current_pos[1] + velo[1])
    sign_x: int = 1 if velo[0] >= 0 else -1
    new_velo: pos_t = (sign_x*(max(0, abs(velo[0]) - 1)), velo[1] - 1)
    return target_pos, new_velo


def single_shot(start: pos_t, velo: pos_t, target_area: dict[str, span_t]) -> tuple[list[pos_t], bool]:
    out: list[pos_t] = [pos := start]
    success = False
    while ((out[-1][1] > target_area['y'][0]) or velo[1] > 0) and (not success):
        pos, velo = _single_shot_move(pos, velo)
        out.append(pos)
        success = check_hit(out[-1], target_area)
    return out, success


def find_highest(positions: list[pos_t]) -> int: return max([pos[1] for pos in positions])


def find_velo_part_1(start: pos_t, target_area: dict[str, span_t]) -> tuple[pos_t, int]:
    velo: pos_t = (-1, -1)
    delta_y: list[int] = [1]
    latest_success_velo: pos_t = velo
    highest: int = -1
    while True:
        print(f'try delta_y[-1]: {delta_y[-1]}')  # | y = {y}')
        for delta_x in range(1, target_area['x'][0]//2):
            velo = (delta_x, delta_y[-1])
            if (_tmp := single_shot(start, velo, target_area))[1]:
                latest_success_velo = velo
                highest = find_highest(_tmp[0])
                delta_y.append(2*delta_y[-1])
                break
        else:
            tmp_y: int = delta_y[-1] - 1
            if tmp_y > latest_success_velo[1]: delta_y.append(tmp_y)
            else: break
    return latest_success_velo, highest


def find_velo_part_2(start: pos_t, target_area: dict[str, span_t], y_upper_cap: int) -> set[pos_t]:
    success_velo: set[pos_t] = set()
    for delta_y in range(target_area['y'][0], y_upper_cap + 1):
        print(f'try delta_y: {delta_y}')
        for delta_x in range(1, target_area['x'][1] + 1):
            velo = (delta_x, delta_y)
            if (_tmp := single_shot(start, velo, target_area))[1]:
                success_velo.add(velo)
    return success_velo


def prettyprint(start: pos_t, positions: list[pos_t], target_area: dict[str, span_t]) -> None:
    span_x: span_t = [min(target_area['y'][0], start[0], 0), max(target_area['y'][1], start[0], 0)]
    span_y: span_t = [min(target_area['x'][0], start[1], 0), max(target_area['x'][1], start[1], 0)]
    for pos in positions:
        span_x = [min(span_x[0], pos[1]), max(span_x[1], pos[1])]
        span_y = [min(span_y[0], pos[0]), max(span_y[1], pos[0])]

    out: str = ''
    for i in range(span_x[1], span_x[0] - 1, -1):
        out += '\n'
        for j in range(span_y[0], span_y[1] + 1):
            if (i, j) == start: out += 'S'
            elif (j, i) in positions: out += '#'
            elif (i >= target_area['y'][0]) and (i <= target_area['y'][1]) and \
                    (j >= target_area['x'][0]) and (j <= target_area['x'][1]): out += 'T'
            else: out += '.'

    print(out)


def main():
    '''
        -- resources & part one --
    '''
    area_ex: dict[str, span_t] = convert_raw_to_dict(RAW_ex)
    # prettyprint((0, 0), single_shot((0, 0), (7, 2), area_ex)[0], area_ex)  # debug
    assert find_velo_part_1((0, 0), area_ex)[0] == (6, 9)

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read().strip()
    area: dict[str, span_t] = convert_raw_to_dict(RAW)
    report: tuple[pos_t, int] = find_velo_part_1((0, 0), area)
    print(report)

    '''
        -- part two --
    '''
    assert len(find_velo_part_2((0, 0), area_ex, y_upper_cap = 9)) == 112
    print(len(find_velo_part_2((0, 0), area, y_upper_cap = report[0][1])))


if __name__ == '__main__': main()
