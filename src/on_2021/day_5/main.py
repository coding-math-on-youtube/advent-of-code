from itertools import repeat


RAW_example = '''0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
'''


class SimpleSparseMat(object):
    data: dict[tuple[int, int], int] = None
    shape: tuple[int, int] = None

    def __init__(self):
        self.data = dict()
        self.shape = 0, 0  # num rows, num cols

    def __getitem__(self, item: tuple[int, int]) -> int: return self.data.get(item, 0)

    def __setitem__(self, item: tuple[int, int], val: int) -> None:
        self.shape = max(self.shape[0], item[0] + 1), max(self.shape[1], item[1] + 1)
        self.data[item] = val

    def __str__(self) -> str:
        out: str = ''
        for i in range(self.shape[0]):
            out += '\n'
            for j in range(self.shape[1]): out += f' {self.data.get((i, j), ".")}'
        return out


def convert_raw_to_mat(lines: list[str], part_2: bool = False) -> SimpleSparseMat:
    m = SimpleSparseMat()
    for line in lines:
        pairs = line.split(' -> ')
        x, y = [], []
        for idx in range(2):
            _x, _y = pairs[idx].split(',')
            x.append(int(_x))
            y.append(int(_y))

        if x[0] == x[1]: v, w = repeat(x[0]), range(min(y[0], y[1]), max(y[0], y[1]) + 1)
        elif y[0] == y[1]: v, w = range(min(x[0], x[1]), max(x[0], x[1]) + 1), repeat(y[0])
        elif part_2:
            step_v = 1 if x[0] <= x[1] else -1
            step_w = 1 if y[0] <= y[1] else -1
            v = range(x[0], x[1] + step_v, step_v)
            w = range(y[0], y[1] + step_w, step_w)
        else: continue

        for vi, wi in zip(v, w): m[vi, wi] = m[vi, wi] + 1

    return m


def analyze(mat: SimpleSparseMat) -> int:
    out: int = 0
    for item, val in mat.data.items():
        if val > 1: out += 1
    return out


def main():
    '''
        -- resources --
    '''
    lines_ex = RAW_example.splitlines()
    with open('input_a', mode = 'r') as fStream:
        lines = fStream.read().splitlines()

    '''
        -- part one --
    '''
    m_ex = convert_raw_to_mat(lines_ex, part_2 = False)
    m = convert_raw_to_mat(lines, part_2 = False)

    assert analyze(m_ex) == 5

    print(analyze(m))

    '''
        -- part two --
    '''
    m_ex = convert_raw_to_mat(lines_ex, part_2 = True)
    m = convert_raw_to_mat(lines, part_2 = True)

    assert analyze(m_ex) == 12

    print(analyze(m))


if __name__ == '__main__': main()
