import numpy as np

from typing import Optional


RAW_ex: str = '''2199943210
3987894921
9856789892
8767896789
9899965678
'''


pos_t: type = tuple[int, int]


def convert_to_arr(lines: list[str]): return np.array([[int(entry) for entry in line] for line in lines])


def find_all_global_minimizer(arr: np.ndarray) -> list[pos_t]:
    lows: list[pos_t] = []
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            adjacents: list[int] = []
            if i > 0: adjacents.append(arr[i - 1, j])
            if i + 1 < arr.shape[0]: adjacents.append(arr[i + 1, j])
            if j > 0: adjacents.append(arr[i, j - 1])
            if j + 1 < arr.shape[1]: adjacents.append(arr[i, j + 1])
            if arr[i, j] < min(adjacents): lows.append((i, j))
    return lows


def evacuate_into_basin(point: pos_t, arr: np.ndarray, basin: Optional[set[pos_t]] = None) -> set[pos_t]:
    if basin is None: basin = set()
    i, j = point
    to_check: list[pos_t] = []
    if i > 0: to_check.append((i - 1, j))
    if i + 1 < arr.shape[0]: to_check.append((i + 1, j))
    if j > 0: to_check.append((i, j - 1))
    if j + 1 < arr.shape[1]: to_check.append((i, j + 1))
    for adjacent in to_check:
        if (arr[adjacent] < 9) and (not (adjacent in basin)):
            basin.add(adjacent)
            evacuate_into_basin(adjacent, arr, basin)
    return basin


def main():
    '''
        -- resources --
    '''
    arr_ex = convert_to_arr(RAW_ex.splitlines())
    lows_ex = find_all_global_minimizer(arr_ex)

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    arr = convert_to_arr(RAW.splitlines())
    lows = find_all_global_minimizer(arr)

    '''
        -- part one --
    '''
    assert len(lows_ex) + sum(arr_ex[i, j] for i, j in lows_ex) == 15
    print(len(lows) + sum(arr[i, j] for i, j in lows))

    '''
        -- part two --
    '''
    all_basins_size_ex = [len(evacuate_into_basin(low_ex, arr_ex)) for low_ex in lows_ex]
    all_basins_size_ex.sort()
    assert all_basins_size_ex[-1]*all_basins_size_ex[-2]*all_basins_size_ex[-3] == 1134

    all_basins_size = [len(evacuate_into_basin(low, arr)) for low in lows]
    all_basins_size.sort()
    print(all_basins_size[-1]*all_basins_size[-2]*all_basins_size[-3])


if __name__ == '__main__': main()
