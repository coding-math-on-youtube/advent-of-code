from math import ceil, floor

from typing import TypeAlias, Union, Optional, Callable, Iterator


RAW_ex: str = '''[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]
'''

RAW_expected_ex: str = '[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]'


class SnailNum(object):
    parent: 'SnailNum' = None

    l: Union['SnailNum', int] = None
    r: Union['SnailNum', int] = None

    @classmethod
    def from_string(cls, num: str, reduce: bool = True) -> 'SnailNum':
        num = num.strip()
        if (len(num) < 5) or (num[0] != '[') or (num[-1] != ']'): raise AssertionError('found ill shaped "number"!')

        num = num[1:-1]
        count_brackets: int = 0
        max_count_brackets: list[int] = [0, 0]
        idx_max_count: int = 0
        center: int = -1
        for idx, entry in enumerate(num):
            match entry:
                case '[':
                    count_brackets += 1
                    max_count_brackets[idx_max_count] = max(max_count_brackets[idx_max_count], count_brackets)
                case ']': count_brackets -= 1
                case ',':
                    if count_brackets == 0:
                        if center == -1:
                            center = idx
                            idx_max_count = 1
                        else: raise AssertionError('found "number" with more than 2 fields!')
                case ' ': continue
                case _:
                    if not (entry in '0123456789'): raise AssertionError('found illegal character!')
        if center == -1: AssertionError('found "number" with less than 2 fields!')

        l: Union[int, 'SnailNum'] = -1
        if max_count_brackets[0] == 0: l = int(num[:center])
        else: l = cls.from_string(num[:center], reduce = False)

        r: Union[int, 'SnailNum'] = -1
        if max_count_brackets[1] == 0: r = int(num[center + 1:])
        else: r = cls.from_string(num[center + 1:], reduce = False)

        out = SnailNum(l, r)
        if reduce: out.reduce()
        return out

    def __init__(self, l: Union['SnailNum', int], r: Union['SnailNum', int]):
        self.l = l
        self.r = r

        for child in (self.l, self.r):
            if isinstance(child, SnailNum): child.parent = self

    def __len__(self) -> int:
        _len: int = 0
        for member in (self.l, self.r):
            if isinstance(member, int): _len += 1
            else: _len += len(member)
        return _len

    def __getitem__(self, item: int) -> int:
        if item < 0: item += len(self)
        if (item < 0) or (item >= len(self)): raise AssertionError('!')

        if isinstance(self.l, int):
            if item == 0: return self.l
            item -= 1
        else:
            sub_len: int = len(self.l)
            if item < sub_len: return self.l[item]
            item -= sub_len

        if isinstance(self.r, int):
            if item == 0: return self.r
            item -= 1
        else: return self.r[item]

    def __setitem__(self, item: int, value: Union['SnailNum', int]) -> None:
        if item < 0: item += len(self)
        if (item < 0) or (item >= len(self)): raise AssertionError('!')

        if isinstance(self.l, int):
            if item == 0:
                self.l = value
                return None
            item -= 1
        else:
            sub_len: int = len(self.l)
            if item < sub_len:
                self.l[item] = value
                return None
            item -= sub_len

        if isinstance(self.r, int):
            if item == 0:
                self.r = value
                return None
            item -= 1
        else: self.r[item] = value

    def __add__(self, other: Union['SnailNum', int]) -> 'SnailNum':
        out = SnailNum(SnailNum.from_string(repr(self)), SnailNum.from_string(repr(other)))
        out.reduce()
        return out

    def __eq__(self, other: 'SnailNum') -> bool:
        if self.depth != other.depth: return False
        if len(self) != len(other): return False
        _iter = other.iterate_leafs()
        for leaf_self, idx_self in self.iterate_leafs():
            try: leaf_other, idx_other = next(_iter)
            except StopIteration: return False
            if leaf_self[idx_self] != leaf_other[idx_other]: return False
        try:
            _ = next(_iter)  # is empty if "self == other"
            return False
        except StopIteration: pass
        return True

    def __str__(self) -> str: return f'[{str(self.l)},{str(self.r)}]'

    def __repr__(self) -> str: return f'[{repr(self.l)},{repr(self.r)}]'

    @property
    def atomic(self) -> bool:
        if isinstance(self.l, SnailNum): return False
        if isinstance(self.r, SnailNum): return False
        return True

    @property
    def lvl(self) -> int:
        if self.parent is None: return 1
        return self.parent.lvl + 1

    @property
    def depth(self) -> int:
        _depth: int = 1
        if isinstance(self.l, SnailNum): _depth = self.l.depth + 1
        if isinstance(self.r, SnailNum): _depth = max(_depth, self.r.depth + 1)
        return _depth

    def iterate_leafs(self) -> Iterator[tuple['SnailNum', int]]:
        l_is_int: bool = isinstance(self.l, int)
        if l_is_int: yield self, 0
        else: yield from self.l.iterate_leafs()
        if isinstance(self.r, int):
            if l_is_int: yield self, 1
            else: yield self, len(self.l)
        else: yield from self.r.iterate_leafs()

    def explode(self) -> bool:
        if self.depth < 5: return False

        node: Union['SnailNum', None] = None
        for node, _ in self.iterate_leafs():
            if node.atomic and (node.lvl > 4): break
        # else: return False

        c_node = node
        p_node = c_node.parent
        node_start_idx: int = 0
        while True:
            if c_node is p_node.r:
                l: Union['SnailNum', int] = p_node.l
                if isinstance(l, int): node_start_idx += 1
                else: node_start_idx += len(l)
            c_node = p_node
            p_node = p_node.parent
            if not p_node: break

        if node_start_idx > 0: self[node_start_idx - 1] += node.l
        node_start_idx += 1
        if node_start_idx < len(self) - 1: self[node_start_idx + 1] += node.r
        p_node = node.parent
        if node is p_node.l: p_node.l = 0
        else: p_node.r = 0

        return True

    def split(self) -> bool:
        entry: int = -1
        for leaf, idx in self.iterate_leafs():
            if (entry := leaf[idx]) > 9: break
        else: return False
        new_node: SnailNum = SnailNum(floor(entry/2), ceil(entry/2))
        if idx == 0: leaf.l = new_node
        else: leaf.r = new_node
        new_node.parent = leaf

        return True

    def reduce(self) -> None:
        while True:
            if self.explode(): continue
            if self.split(): continue
            break

    @property
    def magnitude(self) -> int:
        _mag: int = 0
        if isinstance(self.l, int): _mag = 3*self.l
        else: _mag = 3*self.l.magnitude
        if isinstance(self.r, int): _mag += 2*self.r
        else: _mag += 2*self.r.magnitude
        return _mag


def main():
    # # num_a, num_b = SnailNum(1, 2), SnailNum(3, 4)
    # # num = num_a + (num_c := (num_b + 5))
    # # print(num, num.lvl, num_b.lvl)
    # # print(num_b.l, len(num))
    # # print(num[0], num[1], num[2], num[3], num[4])
    # # num[4] = 99
    # # print(num)
    # # print(num_b.ll)
    # # print(num_c.ll)
    # # print(num_a.ll)
    # # print(num_b.rr)
    # # print(num_c.rr)
    # # print(num_a.rr)
    # # print(num_b.depth, num.depth)
    # #
    # # print(SnailNum.from_string('[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]'))
    #
    # num: SnailNum = SnailNum.from_string('[[[[[9,8],1],2],3],4]')
    # print(num)
    # num.explode()
    # print(num, end = '\n\n')
    #
    # num: SnailNum = SnailNum.from_string('[7,[6,[5,[4,[3,2]]]]]')
    # print(num)
    # num.explode()
    # print(num, end = '\n\n')
    #
    # num: SnailNum = SnailNum.from_string('[[6,[5,[4,[3,2]]]],1]')
    # print(num)
    # num.explode()
    # print(num, end = '\n\n')
    #
    # num: SnailNum = SnailNum.from_string('[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]')
    # print(num)
    # num.explode()
    # print(num, end = '\n\n')
    #
    # num: SnailNum = SnailNum.from_string('[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]')
    # print(num)
    # num.explode()
    # print(num, end = '\n\n')

    # summand1 = SnailNum.from_string('[[[[4,3],4],4],[7,[[8,4],9]]]')
    # summand2 = SnailNum.from_string('[1,1]')
    # print(summand1)
    # print(summand2)
    # out = summand1 + summand2
    # print(out)

    '''
        -- part one --
    '''
    nums_ex: list[SnailNum] = [SnailNum.from_string(line) for line in RAW_ex.splitlines()]
    sum_ex: SnailNum = nums_ex[0]
    for summand_ex in nums_ex[1:]: sum_ex = sum_ex + summand_ex
    expected_ex = SnailNum.from_string(RAW_expected_ex)
    assert sum_ex == expected_ex
    assert sum_ex.magnitude == 4140

    with open('input_a', mode = 'r') as fStream: RAW: str = fStream.read()
    nums: list[SnailNum] = [SnailNum.from_string(line) for line in RAW.splitlines()]
    sum_total: SnailNum = nums[0]
    for summand in nums[1:]: sum_total = sum_total + summand
    print(sum_total.magnitude)


    '''
        -- part two --
    '''
    max_magnitude_ex: int = -1
    for idx_l, summand_ex_l in enumerate(nums_ex):
        for idx_r, summand_ex_r in enumerate(nums_ex):
            if idx_l == idx_r: continue
            sum_pair_ex = summand_ex_l + summand_ex_r
            max_magnitude_ex = max(max_magnitude_ex, sum_pair_ex.magnitude)
    assert max_magnitude_ex == 3993

    max_magnitude: int = -1
    for idx_l, summand_l in enumerate(nums):
        for idx_r, summand_r in enumerate(nums):
            if idx_l == idx_r: continue
            sum_pair = summand_l + summand_r
            max_magnitude = max(max_magnitude, sum_pair.magnitude)
    print(max_magnitude)


if __name__ == '__main__': main()
