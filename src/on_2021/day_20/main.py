from itertools import product

from typing import TypeAlias, Union

from collections.abc import Sequence


RAW_ex: str = '''..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###
'''


def convert_raw(raw: str):
    lines = raw.splitlines()
    img_inst = CompactImg()
    img_inst.enhance_algo = lines[0]
    img_raw = lines[2:]
    for i, line in enumerate(img_raw):
        for j, entry in enumerate(line):
            if entry == '#': img_inst[(i, j)] = 1
    return img_inst


def bin_list_2_int(binary: list[int]) -> int:
    out: int = 0
    for b in binary: out = (out << 1) | b
    return out


coord_t: TypeAlias = tuple[int, int]
shape_t: TypeAlias = Sequence[int]


class CompactImg(object):
    sp_data: dict[coord_t, int] = None  # M[i, j] = 99  <==>  sp_data = {..., (i, j): 99, ...}
    enhance_algo: str = None
    out_lit: bool = False

    _top_left: Union[coord_t, tuple[None, None]] = None
    _bot_right: Union[coord_t, tuple[None, None]] = None

    def __init__(self):
        self.sp_data = {}
        self.out_lit = False
        self._top_left = (False, False)
        self._bot_right = (False, False)
        
    @property
    def top_left(self) -> Union[coord_t, tuple[None, None]]:
        if self._top_left[0] is None:
            min_i: Union[int, None] = None
            min_j: Union[int, None] = None
            for key, value in self.sp_data.items():
                if value != 0:
                    if min_i is None:
                        min_i, min_j = key
                    else:
                        if key[0] < min_i: min_i = key[0]
                        if key[1] < min_j: min_j = key[1]
            self._top_left = min_i, min_j
        return self._top_left
        
    @property
    def bot_right(self) -> Union[coord_t, tuple[None, None]]:
        if self._bot_right[0] is None:
            max_i: Union[int, None] = None
            max_j: Union[int, None] = None
            for key, value in self.sp_data.items():
                if value != 0:
                    if max_i is None:
                        max_i, max_j = key
                    else:
                        if key[0] > max_i: max_i = key[0]
                        if key[1] > max_j: max_j = key[1]
            self._bot_right = max_i, max_j
        return self._bot_right

    @property
    def shape(self) -> Union[shape_t, tuple[None, None]]:
        top_left = self.top_left
        bot_right = self.bot_right
        if top_left[0] is None: return None, None
        return 1 + bot_right[0] - top_left[0], 1 + bot_right[1] - top_left[1]

    def __getitem__(self, item: coord_t) -> int:
        if item in self.sp_data: return self.sp_data[item]
        top_left = self.top_left
        bot_right = self.bot_right
        if self.out_lit and ((item[0] < top_left[0]) or (item[0] > bot_right[0])): return 1
        if self.out_lit and ((item[1] < top_left[1]) or (item[1] > bot_right[1])): return 1
        return 0

    def __setitem__(self, item: coord_t, value: int) -> None:
        self._top_left = (None, None)
        self._bot_right = (None, None)
        self.sp_data[item] = value

    def get_frame_3x3(self, item: coord_t) -> list[int]:
        _i, _j = item
        out: list[int] = []
        for ij in product(range(_i - 1, _i + 2), range(_j - 1, _j + 2)): out.append(self[ij])
        return out

    def enhance(self) -> 'CompactImg':
        new_inst = CompactImg()
        new_inst.enhance_algo = self.enhance_algo
        if self.out_lit and (self.enhance_algo[-1] == '.'): new_inst.out_lit = False
        if (not self.out_lit) and (self.enhance_algo[0] == '#'): new_inst.out_lit = True
        top_left = self.top_left
        bot_right = self.bot_right
        for i in range(top_left[0] - 1, bot_right[0] + 2):
            for j in range(top_left[1] - 1, bot_right[1] + 2):
                ij = (i, j)
                _tmp = self.get_frame_3x3(ij)
                address: int = bin_list_2_int(_tmp)
                new_inst[ij] = 1 if self.enhance_algo[address] == '#' else 0
        return new_inst

    def count_lit(self) -> int:
        out: int = 0
        top_left = self.top_left
        bot_right = self.bot_right
        for i in range(top_left[0], bot_right[0] + 1):
            for j in range(top_left[1], bot_right[1] + 1):
                if self[(i, j)]: out += 1
        return out

    def __str__(self) -> str:
        out: str = ''
        top_left = self.top_left
        bot_right = self.bot_right
        for i in range(top_left[0], bot_right[0] + 1):
            out += '\n'
            for j in range(top_left[1], bot_right[1] + 1):
                out += str(self[(i, j)]) if self[(i, j)] else '.'
        return out

    def __repr__(self) -> str: return self.__str__()


def main():
    '''
        -- part one --
    '''
    img_ex = convert_raw(RAW_ex)
    img_ex = img_ex.enhance()
    img_ex = img_ex.enhance()
    assert img_ex.count_lit() == 35

    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    img = convert_raw(RAW)
    img = img.enhance()
    img = img.enhance()
    print(img.count_lit())

    '''
        -- part two --
    '''
    for _ in range(48): img_ex = img_ex.enhance()
    assert img_ex.count_lit() == 3351

    for _ in range(48): img = img.enhance()
    print(img.count_lit())


if __name__ == '__main__': main()
