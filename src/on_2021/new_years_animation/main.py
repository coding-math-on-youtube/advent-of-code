import curses
from curses import wrapper

import time

import random


class flower(object):
    data: str = r'''
               *    *
   *         '       *       .  *   '     .           * *
                                                               '
       *                *'          *          *        '
   .           *               |               /
               '.         |    |      '       |   '     *
                 \*        \   \             /
       '          \     '* |    |  *        |*                *  *
            *      `.       \   |     *     /    *      '
  .                  \      |   \          /               *
     *'  *     '      \      \   '.       |
        -._            `                  /         *
  ' '      ``._   *                           '          .      '
   *           *\*          * .   .      *
*  '        *    `-._                       .         _..:='        *
             .  '      *       *    *   .       _.:--'
          *           .     .     *         .-'         *
   .               '             . '   *           *         .
  *       ___.-=--..-._     *                '               '
                                  *       *
                *        _.'  .'       `.        '  *             *
     *              *_.-'   .'            `.               *
                   .'                       `._             *  '
   '       '                        .       .  `.     .
       .                      *                  `
               *        '             '                          .
     .                          *        .           *  *
             *        .                                    '
'''  # art by: lgbeard; url: https://www.asciiart.eu/holiday-and-events/fireworks; accessed: 2021-december-31st

    def __init__(self, top, left, stdscr, color):
        self.left = left
        self.top = top

        self.color = color

        self.stdscr = stdscr
        self.h, self.w = self.stdscr.getmaxyx()

        num_rows = 0
        num_cols = 0
        for row in self.data.splitlines():
            num_rows += 1
            num_cols = max(num_cols, len(row))
        self.num_rows = num_rows
        self.num_cols = num_cols
        self.center = (num_rows//2, num_cols//2)
        self.frame = -1

    def to_screen(self) -> None:
        left = self.left
        top = self.top
        self.frame += 1

        x_radius = (max(1, self.center[1] - self.frame),
                    min(self.center[1] + self.frame,
                        self.num_cols,
                        self.w - left - 1))
        y_radius = (max(1, self.center[0] - self.frame),
                    min(self.center[0] + self.frame,
                        self.num_rows,
                        self.h - top - 1))

        for idx, row in enumerate(self.data.splitlines()):
            if idx >= y_radius[1]: break
            if idx >= y_radius[0]:
                for j in range(*x_radius):
                    if j < len(row):
                        entry = row[j]
                        if not entry == ' ':
                            self.stdscr.addstr(top + idx, left + j, entry, self.color)
                    else: break


def anim(stdscr):
    curses.curs_set(False)  # no blinking curser

    curses.use_default_colors()
    curses.init_pair(1, curses.COLOR_RED, -1)
    curses.init_pair(2, curses.COLOR_GREEN, -1)
    curses.init_pair(3, curses.COLOR_BLUE, -1)

    red = curses.color_pair(1)
    green = curses.color_pair(2)
    blue = curses.color_pair(3)
    colors = (blue, green, red)

    fls = []
    height, width = stdscr.getmaxyx()

    stdscr.nodelay(True)
    button_press: int = -1
    to_delete = False
    for frame in range(15000):
        if button_press >= 0: break

        stdscr.clear()

        if to_delete:
            to_delete = False
            _fl = fls[0]
            fls = fls[1:]
            del _fl

        if frame%10 == 0: fls.append(flower(random.randint(0, height),
                                            random.randint(0, width),
                                            stdscr,
                                            colors[random.randint(0, 2)]))

        if len(fls) > 10: to_delete = True
        for idx, fl in enumerate(fls): fl.to_screen()

        time.sleep(0.1)
        stdscr.refresh()

        button_press = stdscr.getch()


def main():
    wrapper(anim)

    print('script finished')


if __name__ == '__main__': main()
