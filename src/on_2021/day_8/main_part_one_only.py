from typing import Union, TypeAlias


RAW_ex_simple: str = 'acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf'

RAW_ex: str = '''be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce
'''

'''
    0:  0    1:  -   2:  0   3:  0   4:  -   5:  0   6:  0   7:  0   8:  0   9:  0
       1 2      - 2     - 2     - 2     1 2     1 -     1 -     - 2     1 2     1 2
        -        -       3       3       3       3       3       -       3       3
       4 5      - 5     4 -     - 5     - 5     - 5     4 5     - 5     4 5     - 5
        6        -       6       6       -       6       6       -       6       6

        6       (2)      5       5      (4)      5       6      (3)     (7)      6
'''


def count_special_digits(lines: list[str]) -> int:
    out: int = 0
    for line in lines:
        rhs: str = line.split(' | ')[1]
        digits: list[str] = rhs.split()
        for digit in digits:
            if len(digit) in [2, 3, 4, 7]: out += 1
    return out


'''
    -- (importable) resources --
'''
lines_ex_simple: list[str] = RAW_ex_simple.splitlines()
lines_ex: list[str] = RAW_ex.splitlines()
with open('input_a', mode = 'r') as fStream:
    lines = fStream.read().splitlines()


def main():
    assert count_special_digits(lines_ex) == 26
    print(count_special_digits(lines))


if __name__ == '__main__': main()
