from typing import Union


RAW_ex = '''16,1,2,0,4,2,7,1,2,14
'''


def find_pos(positions: list[int]) -> tuple[int, int]:
    low, high = min(positions), max(positions)

    fuel_min: int = -1
    x_min: int = low - 1

    for x in range(low, high + 1):
        fuel: int = 0
        for pos in positions: fuel += abs(pos - x)
        if (fuel_min == -1) or fuel < fuel_min:
            fuel_min = fuel
            x_min = x
    return x_min, fuel_min


def find_pos_part_2(positions: list[int]) -> tuple[int, int]:
    low, high = min(positions), max(positions)

    fuel_min: int = -1
    x_min: int = low - 1

    for x in range(low, high + 1):
        fuel: int = 0
        for pos in positions:
            distance: int = abs(pos - x)
            fuel += (distance*(distance + 1))//2  # summing formula of Gauss
        if (fuel_min == -1) or fuel < fuel_min:
            fuel_min = fuel
            x_min = x
    return x_min, fuel_min


def main():
    '''
        -- resources --
    '''
    positions_ex = [int(entry) for entry in RAW_ex.strip().split(',')]
    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    positions = [int(entry) for entry in RAW.strip().split(',')]

    '''
        -- part one --
    '''
    x_min_ex, fuel_min_ex = find_pos(positions_ex)
    assert x_min_ex == 2
    assert fuel_min_ex == 37
    print(find_pos(positions))

    '''
        -- part two --
    '''
    x_min_ex, fuel_min_ex = find_pos_part_2(positions_ex)
    assert x_min_ex == 5
    assert fuel_min_ex == 168
    print(find_pos_part_2(positions))


if __name__ == '__main__': main()
