'''
    special thanks:
    ---------------

    this second version of day 7 has been accomplished by using GEKKO, which is wrapper around APMonitor
    -> see "https://gekko.readthedocs.io/en/latest/"
    -> see "https://apmonitor.com/"

    further readings:
    -----------------

    "GEKKO" article:
        @article{beal2018gekko,
                 author={Beal, Logan and Hill, Daniel and Martin, R and Hedengren, John},
                 title={GEKKO Optimization Suite},
                 journal={Processes},
                 volume={6},
                 number={8},
                 pages={106},
                 year={2018},
                 doi={10.3390/pr6080106},
                 publisher={Multidisciplinary Digital Publishing Institute}}

    "APMonitor" article:
        @article{Hedengren2014,
                 author = "John D. Hedengren and Reza Asgharzadeh Shishavan and Kody M. Powell and Thomas F. Edgar",
                 title = "Nonlinear modeling, estimation and predictive control in {APMonitor}",
                 journal = "Computers \& Chemical Engineering ",
                 volume = "70",
                 number = "",
                 pages = "133 - 148",
                 year = "2014",
                 note = "Manfred Morari Special Issue ",
                 issn = "0098-1354",
                 doi = "http://dx.doi.org/10.1016/j.compchemeng.2014.04.013"}
'''

import gekko


RAW_ex = '''16,1,2,0,4,2,7,1,2,14
'''


def find_pos(positions: list[int]) -> tuple[int, int]:
    low, high = min(positions), max(positions)

    m = gekko.GEKKO(remote = False)
    target_pos = m.Var(lb = low, ub = high, integer = True, name = f'target_pos')
    for pos in positions:
        eqn = abs(target_pos - pos)
        m.Minimize(eqn)
    m.options.SOLVER = 1
    m.solve(disp = False)
    return int(target_pos.value[0]), int(m.options.OBJFCNVAL)


def find_pos_part_2(positions: list[int]) -> tuple[int, int]:
    low, high = min(positions), max(positions)

    m = gekko.GEKKO(remote = False)
    target_pos = m.Var(lb = low, ub = high, integer = True, name = f'target_pos')
    for pos in positions:
        distance: int = abs(target_pos - pos)
        eqn = (distance*(distance + 1))/2  # summing formula of Gauss
        m.Minimize(eqn)
    m.options.SOLVER = 1
    m.solver_options = ['minlp_branch_method 1']  # 1 = depth first, 2 = breadth first
    m.solve(disp = False)
    return int(target_pos.value[0]), int(m.options.OBJFCNVAL)


def target_func_part_2(target_pos: int, positions: list[int]):
    out: int = 0
    for pos in positions:
        distance: int = abs(target_pos - pos)
        out += (distance*(distance + 1))/2  # summing formula of Gauss
    return out


def main():
    '''
        -- resources --
    '''
    positions_ex = [int(entry) for entry in RAW_ex.strip().split(',')]
    with open('input_a', mode = 'r') as fStream: RAW = fStream.read()
    positions = [int(entry) for entry in RAW.strip().split(',')]

    '''
        -- part one --
    '''
    x_min_ex, fuel_min_ex = find_pos(positions_ex)
    assert x_min_ex == 2
    assert fuel_min_ex == 37
    print(find_pos(positions))

    '''
        -- part two --
    '''
    x_min_ex, fuel_min_ex = find_pos_part_2(positions_ex)
    assert x_min_ex == 5
    assert fuel_min_ex == 168
    print(find_pos_part_2(positions))


if __name__ == '__main__': main()
