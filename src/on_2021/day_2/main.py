from typing import Union, Optional, TypeAlias


RAW_example: str = '''forward 5
down 5
forward 8
up 3
down 8
forward 2
'''

Cmds_t: TypeAlias = tuple[tuple[str, int], ...]
Pos_t: TypeAlias = list[int]


def gather_cmds(cmds_raw: str) -> Cmds_t:
    sep = lambda line: (line.split(' ')[0],
                        int(line.split(' ')[1]))
    cmds = tuple(sep(line) for line in cmds_raw.splitlines())
    return cmds


def analyse_cmds(cmds: Cmds_t,
                 start_pos: Optional[Pos_t] = None) -> Pos_t:
    pos: Pos_t = start_pos
    if (pos is None) or (len(pos) != 2): pos = [0, 0]
    for cmd in cmds:
        match cmd:
            case ("forward", x): pos[0] += x
            case ("up", x): pos[1] -= x
            case ("down", x): pos[1] += x
            case _: raise AssertionError('?')
    return pos


def analyse_cmds_alt(cmds: Cmds_t,
                     start_pos: Optional[Pos_t] = None) -> Pos_t:
    pos: Pos_t = start_pos
    if (pos is None) or (len(pos) != 3): pos = [0, 0, 0]
    for cmd in cmds:
        match cmd:
            case ("forward", x):
                pos[0] += x
                pos[1] += x*pos[2]
            case ("up", x): pos[2] -= x
            case ("down", x): pos[2] += x
            case _: raise AssertionError('?')
    return pos[:2]


def main():
    '''
        gather resources
    '''
    cmds_example: Cmds_t = gather_cmds(RAW_example)
    with open('input_a', mode = 'r') as fStream:
        RAW = fStream.read()
    cmds: Cmds_t = gather_cmds(RAW)

    # '''
    #     -- part one --
    # '''
    # assert analyse_cmds(cmds_example) == [15, 10]
    # pos : Pos_t = analyse_cmds(cmds)
    # print(pos, pos[0]*pos[1])

    '''
        -- part two --
    '''
    assert analyse_cmds_alt(cmds_example) == [15, 60]
    pos: Pos_t = analyse_cmds_alt(cmds)
    print(pos, pos[0]*pos[1])


if __name__ == '__main__': main()
